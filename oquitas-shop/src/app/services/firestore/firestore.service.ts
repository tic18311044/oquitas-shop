import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class FirestoreService {

  constructor(
    private firestore: AngularFirestore
  ) {}

  public createProd(data: any) {
    return this.firestore.collection('products').add(data);
  }

  public getProd(documentId: string) {
    return this.firestore.collection('products').doc(documentId).snapshotChanges();
  }

  public getProds() {
    return this.firestore.collection('products').snapshotChanges();
  }

  public updateProd(documentId: string, data: any) {
    return this.firestore.collection('products').doc(documentId).set(data);
  }

  public deleteProd(documentId: string) {
    return this.firestore.collection('products').doc(documentId).delete();
  }
  public createUser(data: any) {
    return this.firestore.collection('users').add(data);
  }

  public getUser(documentId: string) {
    return this.firestore.collection('users').doc(documentId).snapshotChanges();
  }

  public getUsers() {
    return this.firestore.collection('users').snapshotChanges();
  }

  public updateUser(documentId: string, data: any) {
    return this.firestore.collection('users').doc(documentId).set(data);
  }

  public deleteUser(documentId: string) {
    return this.firestore.collection('products').doc(documentId).delete();
  }
}
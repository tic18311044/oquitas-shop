import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FirestoreService } from '../services/firestore/firestore.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})

export class AdminComponent implements OnDestroy {

  public documentId = null;
  public products = [];
  public currentStatus = 1;
  public newProdForm = new FormGroup({
    Producto: new FormControl('', Validators.required),
    Precio: new FormControl('', Validators.required),
    Stock: new FormControl('', Validators.required),
    Proveedor: new FormControl('', Validators.required),
    id: new FormControl('')
  });

  constructor(
    private firestoreService: FirestoreService
  ) {}
  

  ngOnInit() {
    this.newProdForm.setValue({
      id: '',
      Producto: '',
      Precio: '',
      Stock: '',
      Proveedor: ''
    });
    this.firestoreService.getProds().subscribe((productsSnapshot) => {
      this.products = [];
      productsSnapshot.forEach((catData: any) => {
        this.products.push({
          id: catData.payload.doc.id,
          data: catData.payload.doc.data()
        });
      });
    });
  }

  ngOnDestroy() {

  }
  

  public newProd(form, documentId = this.documentId) {
    if (this.currentStatus === 1) {
      const data = {
        Producto: form.Producto,
        Precio: form.Precio,
        Stock: form.Stock,
        Proveedor: form.Proveedor
      };
      this.firestoreService.createProd(data).then(() => {
        console.log('Documento creado exitósamente!');
        this.newProdForm.setValue({
          Producto: '',
          Precio: '',
          Stock: '',
          Proveedor: ''
        });
      }, (error) => {
        console.error(error);
      });
    } else {
      const data = {
        Producto: form.Producto,
        Precio: form.Precio,
        Stock: form.Stock,
        Proveedor: form.Proveedor
      };
      this.firestoreService.updateProd(documentId, data).then(() => {
        this.currentStatus = 1;
        this.newProdForm.setValue({
          Producto: '',
          Precio: '',
          Stock: '',
          Proveedor: ''
        });
        console.log('Documento editado exitósamente');
      }, (error) => {
        console.log(error);
      });
    }
  }

  public editProd(documentId) {
    const editSubscribe = this.firestoreService.getProd(documentId).subscribe((cat) => {
      this.currentStatus = 2;
      this.documentId = documentId;
      this.newProdForm.setValue({
        id: documentId,
        Producto: cat.payload.data()['Producto'],
        Precio: cat.payload.data()['Precio'],
        Stock: cat.payload.data()['Stock'],
        Proveedor: cat.payload.data()['Proveedor']
      });
      editSubscribe.unsubscribe();
    });
  }

  public deleteProd(documentId) {
    this.firestoreService.deleteProd(documentId).then(() => {
      console.log('Usuario Eliminado!');
    }, (error) => {
      console.error(error);
    });
    
  }
  

  
}
